package com.example.stefano95.rifiutiingombranti.java;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.stefano95.rifiutiingombranti.R;

public class Login extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContextViewAndToolbar(this, R.layout.activity_login,false, "login");

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.attrEmail);
        mPasswordView = (EditText) findViewById(R.id.attrPassword);

        Button mEmailSignInButton = (Button) findViewById(R.id.titleLogin);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        TextView lostPassword = (TextView) findViewById(R.id.titlePasswordDimenticata);
        lostPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                reAskPassword();
            }
        });

        TextView signInLink = (TextView) findViewById(R.id.titlePasswordDimenticata);
        signInLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signInAction();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        /* per caricare i dati aggiornati */
        /*
        final UtentiFactory uf = UtentiFactory.getInstance();

        SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        final String keyUser = settings.getString("keyUser", null);

        if (keyUser != null) {
            DatabaseReference ref = mDatabase.child("Utente");
            ref.orderByChild("key").equalTo(keyUser).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot itemDataSnapshot : dataSnapshot.getChildren()) {
                        Intent i;
                        Utente u = itemDataSnapshot.getValue(Utente.class);
                        uf.setCurrent(u);
                        if (u.getUtente()) {
                            i = new Intent(LoginActivity.this, UtenteActivity.class);
                        } else {
                            i = new Intent(LoginActivity.this, GestoreActivity.class);
                        }

                        InvitoFactory invitoFactory = InvitoFactory.getInstance();
                        invitoFactory.setContext(LoginActivity.this);
                        invitoFactory.startListener(); // carico gli inviti;
                        MessaggiFactory.getInstance().startListener(); // carico i messaggi;
                        AmiciFactory.getInstance().startListener(); // carico gli amici

                        startActivity(i);
                        finish();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG,"Username e password non trovati");
                }
            });
        }
        */
    }

    private void setContextViewAndToolbar(Login login, int activity_login, boolean b, String login1) {
    }

    @Override
    public void onStart() {
        super.onStart();
        // mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        /*
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        */
    }
    private void signInAction() {
        Intent i = new Intent(Login.this, Registrazione.class);
        startActivityForResult(i,1);
    }

    private void reAskPassword() {
        Intent i = new Intent(Login.this, PasswordDimenticata.class);
        startActivityForResult(i,0);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for empty username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private final String mUsername;
        private final String mPassword;
        private Utente u = null;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            /*
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            */

            UtentiFactory uf = UtentiFactory.getInstance();
            u = uf.check(mUsername,mPassword);
            if (u != null) {
                return true;
            }
            else
                Log.d(TAG,"Errore login: username="+mUsername+" password="+mPassword);
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                Intent i = null;

                SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("keyUser",u.getKey());
                editor.commit();

                newUserLogged(u);
                if (u.getUtente()) {
                    i = new Intent(Login.this, UtenteActivity.class);
                }


                MessaggiFactory.getInstance().startListener(); // carico i messaggi;

                startActivity(i);
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_credentials));
                mPasswordView.requestFocus();
            }
        }

        private void newUserLogged(Utente u) {
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}
