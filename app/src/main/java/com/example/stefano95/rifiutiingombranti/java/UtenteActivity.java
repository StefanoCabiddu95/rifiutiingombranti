package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 01/08/2017.
 */

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.stefano95.rifiutiingombranti.R;

public class UtenteActivity extends MyAppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
