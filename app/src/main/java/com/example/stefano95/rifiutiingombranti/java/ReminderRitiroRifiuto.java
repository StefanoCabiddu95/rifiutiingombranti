package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 02/08/2017.
 */

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.stefano95.rifiutiingombranti.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReminderRitiroRifiuto extends AlarmManagerBroadcastReceiver{

    private static final String TAG = "AlarmManagerBroadcast";

    public void onReceive(Context context, Intent intent) {
        SimpleDateFormat sf = new SimpleDateFormat("DD/MM H:mm:s");
        Log.d(TAG,"Allarme ricevuto " + sf.format(new Date()));

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        //Acquire the lock
        wl.acquire();

        //You can do the processing here update the widget/remote views.
        Bundle extras = intent.getExtras();
        String messaggio = extras.getString("msg");

        StringBuilder msgStr = new StringBuilder();
        msgStr.append(messaggio);

        Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();

        Notification(context,msgStr.toString());
        //Release the lock
        wl.release();

    }
    public void Notification(Context context, String message) {
        // Set Notification Title
        String strtitle = context.getString(R.string.alarmprenotation);
        // Open NotificationView Class on Notification Click
        /*
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", message);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        */
        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.ingombro)
                // Set Ticker Message
                .setTicker(message)
                // Set Title
                .setContentTitle(context.getString(R.string.alarmprenotation))
                // Set Text
                .setContentText(message)
                // Add an Action Button below Notification
                // .addAction(R.drawable.ic_launcher, "Action Button", pIntent)
                // Set PendingIntent into Notification
                // .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(2, builder.build());
    }
    public void SetAlarm(Context context, long millisec, String messaggio) {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra("msg",messaggio);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, millisec, pi);

        SimpleDateFormat sf = new SimpleDateFormat("H:mm:s");
        Log.d(TAG,"Allarme impostato per le " + sf.format(new Date(millisec)));
    }

}
