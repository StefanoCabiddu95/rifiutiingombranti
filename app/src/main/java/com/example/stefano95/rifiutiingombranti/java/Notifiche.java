package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 02/08/2017.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.stefano95.rifiutiingombranti.R;

import java.util.ArrayList;

public class Notifiche extends IntentService{
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public Notifiche(String name) {
        super(name);
    }

    private static final int NOTIFICA_CHAT = 0 ;
    String TAG = "NotifierService";
    Notifiche service;
    private String nameCurrentActivity;
    private static int numNotificheMsg = 0;
    public ArrayList<ChildEventListener> listListener;
    private static NotificationManager notificationManager;
    public String msg = "OK";

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
        listListener = new ArrayList<>();
    }

    public Notifiche() {
        super("NotifierService");
        service = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG,"onHandleIntent");
        Utente u = UtentiFactory.getInstance().getCurrent();
        if (u != null) {
            Log.d(TAG,"Utente is not null");
            startMessageListen(u);
        }
        else {
            Log.d(TAG,"Utente is null");
            SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
            final String keyUser = settings.getString("keyUser", null);
            if (keyUser == null) {
                Log.d(TAG,"keyUser is null");
//                UtentiFactory.getInstance().setOnUserLoginListener(new UtentiFactory.UserLoginListener() {
//                    @Override
//                    public void onUserLogin(Utente u) {
//                        startMessageListen(u);
//                    }
//                });
            }
            }
        }

    private void startMessageListen(Utente u) {
    // elimina eventuali notifiche
     if (notificationManager != null) {
            notificationManager.cancel(NOTIFICA_CHAT);
        }
                            if(msg != null)
                            msg = "Hai un nuovo messaggio in chat";
                            else
                                msg = "Hai "+numNotificheMsg+" nuovi messaggi in chat";
        PendingIntent resultPendingIntent = null;
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(service)
                                    .setContentIntent(resultPendingIntent)
                                    .setSmallIcon(R.drawable.ingombro)
                                    .setContentTitle(getString(R.string.app_name)+" - Chat")
                                    .setContentText(msg)
                                    .setAutoCancel(true)
                                    .setNumber(numNotificheMsg);

                            if (numNotificheMsg == 1) {
                                Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                notificationBuilder.setSound(defaultSoundUri);
                            }
                            notificationManager.notify(NOTIFICA_CHAT, notificationBuilder.build());
                        }


            public void onChildChanged(String s) {
                Log.d(TAG,"Nuovo messaggio ricevuto: onChildChanged");
            }


            public void onChildRemoved() {
                Log.d(TAG,"Nuovo messaggio ricevuto: onChildRemoved");
            }


            public void onChildMoved(String s) {
                Log.d(TAG,"Nuovo messaggio ricevuto: onChildMoved");
            }


            public void onCancelled() {
                Log.d(TAG,"Nuovo messaggio ricevuto: onCancelled");
            }

    private final IBinder mBinder = new MyBinder();
    private Messenger outMessenger;

    @Override
    public IBinder onBind(Intent arg0) {
        Bundle extras = arg0.getExtras();
        Log.d(TAG,"onBind");
        // Get messager from the Activity
        if (extras != null) {
            Log.d(TAG,"onBind with extra");
            outMessenger = (Messenger) extras.get("MESSENGER");
        }
        return mBinder;
    }

    public class MyBinder extends Binder {
        Notifiche getService() {
            return service;
        }
    }
    public void setCurrentActivityName(String name) {
        this.nameCurrentActivity = name;
        Log.d(TAG,"Current activity="+name);
        if (name.equals("Chat")) {
            Log.d(TAG,"Numero messaggi chat="+numNotificheMsg);
            if (true || numNotificheMsg > 0) {
                Log.d(TAG,"Cancellazione notifiche");
                if (notificationManager != null)
                    notificationManager.cancel(NOTIFICA_CHAT);
            }
            numNotificheMsg = 0;
        }
    }
    public void unsetCurrentActivityName() {
        this.nameCurrentActivity = null;
        Log.d(TAG,"Current activity=null");
    }
}
