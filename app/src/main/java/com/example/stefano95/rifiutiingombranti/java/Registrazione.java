package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 25/07/2017.
 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.stefano95.rifiutiingombranti.R;

public class Registrazione extends MyAppCompatActivity {

    private static final String TAG = "SignIn";
    private UserSignInTask mAuthTask = null;

    private EditText mNameView;
    private EditText mUsernameView;
    private EditText mEmailView;
    private EditText mPasswordView;
    private RadioGroup mUserTypeView;
    private View mProgressView;
    private View mSignInFormView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContextViewAndToolbar(this, R.layout.activity_registrazione,false,"registrazione");


        mUsernameView = (EditText) findViewById(R.id.attrNomeUtente);
        mEmailView = (EditText) findViewById(R.id.attrEmail);

        mPasswordView = (EditText) findViewById(R.id.attrPassword);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.titleLogin || id == EditorInfo.IME_NULL) {
                    attemptSignIn();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.titleLogin);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignIn();
            }

        });
        mSignInFormView = findViewById(R.id.sign_in_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    public View findViewById(int attrPassword) {
        return null;
    }

    private void setContextViewAndToolbar(Registrazione registrazione, int activity_registrazione, boolean b, String registrazione1) {
    }

    private void attemptSignIn() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mNameView.setError(null);
        mUsernameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String name = mNameView.getText().toString();
        String username = mUsernameView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        int idUserType = mUserTypeView.getCheckedRadioButtonId();
        RadioButton userTypeRadio = (RadioButton) findViewById(idUserType);
        Boolean utente = userTypeRadio.getText().toString().equals("Utente");
        Log.d(TAG,userTypeRadio.getText().toString());

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            if (!cancel) focusView = mUsernameView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            if (!cancel) focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            if (!cancel) focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            if (!cancel) focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new Registrazione.UserSignInTask(name,email,username,password,utente);
            mAuthTask.execute((Void) null);
        }
    }


    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserSignInTask extends AsyncTask<Void, Void, Boolean> {
        private final String mName;
        private final String mUsername;
        private final String mEmail;
        private final String mPassword;
        private final Boolean mUtente;

        UserSignInTask(String name, String email, String username, String password, Boolean utente) {
            mUsername = username;
            mPassword = password;
            mName = name;
            mEmail = email;
            mUtente = utente;
        }

        int NO_DUPLICATED = 0x0;
        int EMAIL_EXIST = 0x1;
        int USERNAME_EXIST = 0x2;

        int error = NO_DUPLICATED;
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            UtentiFactory ul = UtentiFactory.getInstance();
            if (ul.usernameExist(mUsername)) {
                error = USERNAME_EXIST;
            }
            if (ul.emailExist(mEmail)) {
                error = EMAIL_EXIST;
            }
            if (error != NO_DUPLICATED)
                return false;

            Utente u = new Utente();
            u.setUsername(mUsername);
            u.setUtente(mUtente);
            u.setName(mName);
            u.setPassword(mPassword);
            u.setEmail(mEmail);
            ul.add(u);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) finish();
            else {
                if ((error & USERNAME_EXIST) != NO_DUPLICATED) {
                    mUsernameView.setError(getString(R.string.error_duplicate_username));
                    mUsernameView.requestFocus();
                }
                if ((error & EMAIL_EXIST) != NO_DUPLICATED) {
                    mEmailView.setError(getString(R.string.error_duplicate_email));
                    mEmailView.requestFocus();
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public void finish() {
    }

    private boolean showProgress(boolean flag){
        return false;
    }

}
