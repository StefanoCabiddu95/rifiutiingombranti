package com.example.stefano95.rifiutiingombranti.java;

import java.util.ArrayList;

/**
 * Created by Stefano95 on 01/08/2017.
 */

public class MessaggiFactory {

    private ListChangeListener onListChangedListener;

    public static MessaggiFactory getInstance() {
        MessaggiFactory instance = null;
        return instance;
    }

    public void setOnListChangedListener(ListChangeListener onListChangedListener) {
        this.onListChangedListener = onListChangedListener;
    }

    public void inviaMessaggio(String s) {
    }

    public ArrayList getList() {
        ArrayList list = null;
        return list;
    }

    public void unsetOnListChangedListener() {
    }

    public void startListener() {
    }

    public abstract static class ListChangeListener {
        public abstract void onListChanged(ArrayList list);
    }
}
