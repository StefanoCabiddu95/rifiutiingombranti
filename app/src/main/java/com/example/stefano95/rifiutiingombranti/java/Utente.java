package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 01/08/2017.
 */

import java.util.HashMap;
import java.util.Map;

public class Utente {

    private String key;
    private String name;
    private String username;
    private String email;
    private String password;
    private Boolean utente;
    private String keygroup;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getUtente() {
        return utente;
    }

    public void setUtente(Boolean mUtente) {
        this.utente = mUtente;
    }

    public void setPassword(String mPassword) {
        this.password = mPassword;
    }

    public String getPassword() {
        return password;
    }

    public Map<String, Object> toMap() {
        HashMap result = new HashMap();

        result.put("key",key);
        result.put("keygroup",keygroup);
        result.put("username",username);
        result.put("email",email);
        result.put("password",password);
        result.put("name",name);
        result.put("utente",utente);

        return result;
    }

    public String getKeyGroup() {
        return keygroup;
    }

    public void setKeyGroup(String keyGroup) {
        this.keygroup = keyGroup;
    }

}
