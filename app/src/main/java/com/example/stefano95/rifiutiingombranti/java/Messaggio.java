package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 02/08/2017.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class Messaggio {
    private String messaggio;
    private String keyUser;
    private String key;
    private String name;
    private long time;

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public String getKeyUser() {
        return keyUser;
    }

    public void setKeyUser(String keyUser) {
        this.keyUser = keyUser;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
