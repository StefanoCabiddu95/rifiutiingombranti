package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 01/08/2017.
 */

import android.util.Log;

import org.w3c.dom.Attr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.MissingResourceException;

public class UtentiFactory {

    private static UtentiFactory ul;
    private static ArrayList ulist;
    private Utente current;
    private static String TAG = "UtentiFactory";
    private Object DataSnapshot;
    private String s;
//    private UserLoginListener listener;

    public static UtentiFactory getInstance() {
        if (ul == null) {
            ul = new UtentiFactory();
        }
        return ul;
    }
    private UtentiFactory() {
        ulist = new ArrayList();
    }


    public Utente check(String mUsername, String mPassword) {
        Utente u = findUserByUserName(mUsername);
        if (u != null) {
            Log.d(TAG,"trovato utente: "+u.getUsername() + "," + u.getPassword());
            if (u.getPassword().equals(mPassword)) {
                setCurrent(u);
                return u;
            }
        }
        else
            Log.d(TAG,"Non trovato utente: "+mUsername + "," + mPassword);
        return null;
    }

    public void setCurrent(Utente u) {
        current = u;
//        if (listener != null)
//            listener.onUserLogin(u);
    }
    public Utente getCurrent() {
        return current;
    }

    public void add(Utente u) {
        String key = ul.push().getKey();
        u.setKey(key);
        u.setKeyGroup(key);
        ul.child(key).setValue(String.valueOf(u));
    }

    private Attr child(String key) {
        return null;
    }

    private MissingResourceException push() {
        return null;
    }

    public boolean usernameExist(String username) {
        return (findUserByUserName(username) != null);
    }

    public boolean emailExist(String email) {
        return (findUserByEmail(email) != null);
    }

    public void changePassword(String email, String password) {
        Utente u = findUserByEmail(email);
        if (u != null) {
            u.setPassword(password);
            //ul.child(u.getKey()).updatechildren(u.toMap());
        }Chat
                ChatPending;
        ListaMsgAdapter
                Login;
        MessaggiFactory
                MyAppCompatActivity;
        NotifierService
                PasswordDimenticata;
        RecuperoPassword
                Registrazione;
        RifiutoIngombrante
                Utente;
        UtenteActivity
                UtentiFactory;
    }

    public Utente findUserByKey(String key) {
        Iterator i = ulist.iterator();

        while (i.hasNext()) {
            Utente u = (Utente) i.next();
            if (u.getKey().equals(key)) {
                return u;
            }
        }
        return null;
    }

    public Utente findUserByUserName(String username) {
        Iterator i = ulist.iterator();

        while (i.hasNext()) {
            Utente u = (Utente) i.next();
            if (u.getUsername().equals(username)) {
                return u;
            }
        }
        return null;
    }

    public Utente findUserByEmail(String email) {
        Iterator i = ulist.iterator();

        while (i.hasNext()) {
            Utente u = (Utente) i.next();
            if (u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

}
