create schema rifiutiIngombranti;

/*il campo persona denoterà se la persona loggata è utente o gestore*/

create table rifiutiIngombranti.utente(
	id int primary key,
	nomeUtente varchar(64) not null,
	email varchar(256) not null,
	password varchar(256) not null,
	persona char(1) not null
);

create table rifiutiIngombranti.immagini(
	id int primary key,
	nomeImmagine varchar(128) not null,
	urlImmagine varchar(2048) not null,
	data date
);

create table rifiutiIngombranti.carica(
	utente int references rifiutiIngombranti.utente on delete cascade on update cascade,
	immagini int references rifiutiIngombranti.immagini on delete cascade on update cascade,
	data date,
	primary key(utente,immagini)
);