package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 28/07/2017.
 */

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.stefano95.rifiutiingombranti.R;

import java.util.ArrayList;

public class Chat extends MyAppCompatActivity {

    private static final String TAG = "Chat";
    ListView mListView;
    Utente u;
    ImageButton send_button;
    EditText msg;
    ListaMsgAdapter adapter;
    MessaggiFactory mf;

    public MessaggiFactory.ListChangeListener listChangeListener = new MessaggiFactory.ListChangeListener() {
        @Override
        public void onListChanged(ArrayList list) {
            if (adapter != null) {
                adapter.addList(list);
                mListView.setSelection(adapter.getCount() - 1);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContextViewAndToolbar(this, R.layout.activity_chat,false,"chat");

        u = UtentiFactory.getInstance().getCurrent();
        mListView = (ListView) findViewById(R.id.lista_messaggi);
        mf = MessaggiFactory.getInstance();
        mf.setOnListChangedListener(listChangeListener);
        adapter = new ListaMsgAdapter(this,mf.getList());
        mListView.setAdapter((ListAdapter) adapter);
        mListView.setSelection(adapter.getCount() - 1);

        msg = (EditText) findViewById(R.id.attrMessaggio);
        msg.clearFocus();
        send_button = (ImageButton) findViewById(R.id.send_msg_button);

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessaggiFactory mf = MessaggiFactory.getInstance();
                mf.inviaMessaggio(msg.getText().toString());
                msg.setText("");
                adapter.addList(mf.getList());
            }
        });
    }

    @Override
    protected void onRestart() {
        mf.setOnListChangedListener(listChangeListener);
        super.onRestart();
    }

    @Override
    protected void onStop( ){
        mf.unsetOnListChangedListener();
        super.onStop();
    }

}
