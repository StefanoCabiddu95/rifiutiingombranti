package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 02/08/2017.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import com.example.stefano95.rifiutiingombranti.R;

import java.util.Date;

public class SplashHome extends MyAppCompatActivity {

    private static final String TAG = "SplashHome";
    private static int SPLASH_TIME_OUT = 3000;
    Intent i;
    long start;
    private ProgressBar mProgress;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContextViewAndToolbar(this, R.layout.activity_schermata_avvio,false, null);

        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        mProgress.setMax(SPLASH_TIME_OUT);

        start = (new Date()).getTime();

        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus < SPLASH_TIME_OUT) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    long now = (new Date()).getTime();
                    mProgressStatus = (int) (now - start);
                    // Update the progress bar
                    mHandler.post(new Runnable() {
                        public void run() {
                            mProgress.setProgress(mProgressStatus);
                        }
                    });
                }
            }
        }).start();

            }


        SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        final String keyUser = settings.getString("keyUser", null);

    void aspetta(int millisec) {
        if (millisec <= 0) {
            if (i == null)
                i = new Intent(SplashHome.this, Login.class);
            startActivity(i);

            // close this activity
            finish();
        }
        else {
            new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    if (i == null)
                        i = new Intent(SplashHome.this, Login.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }
            }, millisec);
        }
    }

}
