package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 25/07/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.stefano95.rifiutiingombranti.R;

public class PasswordDimenticata extends MyAppCompatActivity{

    private EditText mEmailView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContextViewAndToolbar(this, R.layout.activity_password_dimenticata,false,"recupero password");
        mEmailView = (EditText) findViewById(R.id.attrEmail);

        Button mLostPasswordButton = (Button) findViewById(R.id.password_lost_button);
        mLostPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*
            Intent i = new Intent(Intent.ACTION_SEND);

            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{mEmailView.getText().toString()});
            i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
            i.putExtra(Intent.EXTRA_TEXT   , "body of email");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            }
            catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(LostPassword.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
            */
                mEmailView.setError(null);

                String email = mEmailView.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    mEmailView.setError(getString(R.string.error_field_required));
                    mEmailView.requestFocus();
                }
                else if (!isEmailValid(email)) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                    mEmailView.requestFocus();
                }
                else {
                    if (UtentiFactory.getInstance().emailExist(email)) {
                        Intent i = new Intent(PasswordDimenticata.this,RecuperoPassword.class);
                        i.putExtra("email",email);
                        startActivity(i);
                        finish();
                    }
                    else {
                        mEmailView.setError(getString(R.string.error_email_not_exist));
                        mEmailView.requestFocus();
                    }
                }
            }
        });
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

}
