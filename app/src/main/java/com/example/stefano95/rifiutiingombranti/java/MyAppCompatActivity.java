package com.example.stefano95.rifiutiingombranti.java;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.example.stefano95.rifiutiingombranti.R;

/**
 * Created by Stefano95 on 28/07/2017.
 */

public class MyAppCompatActivity extends AppCompatActivity {
    private static final String TAG = "MyAppCompatActivity";
    NotifierService mService;
    boolean mBound = false;

    protected void logout() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();


        Intent i = new Intent(MyAppCompatActivity.this, Login.class);
        startActivity(i);
        finish();
    }

    protected void newUserLogged(Utente u) {
        if (mBound)
            mService.startMessageListen(u);
    }
    public void setContextViewAndToolbar(final AppCompatActivity a, int id, Boolean upEnabled) {
        setContextViewAndToolbar(a, id, upEnabled, null);
    }
    public void setContextViewAndToolbar(AppCompatActivity a, int id) {
        setContextViewAndToolbar(a, id, true, null);
    }

    public void setContextViewAndToolbar(final AppCompatActivity a, int id, Boolean upEnabled, String nome) {
        a.setContentView(id);


        ActionBar ab = a.getSupportActionBar();
//        ColorDrawable cd = new ColorDrawable(Color.parseColor("#ffffff"));
//        ab.setBackgroundDrawable(cd);

        /*
        if (upEnabled)
            ab.setDisplayHomeAsUpEnabled(true);
        */

        if (!TextUtils.isEmpty(nome))
            ab.setTitle(getString(R.string.app_name) + " (" + nome + ")");

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // ARE WE CONNECTED TO THE NET
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            // ok
        }

    }

}
