package com.example.stefano95.rifiutiingombranti.java;

/**
 * Created by Stefano95 on 25/07/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stefano95.rifiutiingombranti.R;


public class RecuperoPassword extends MyAppCompatActivity{

    private static final String TAG = "InsertNewPassword";
    TextView mPassword;
    String email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContextViewAndToolbar(this, R.layout.activity_recupero_password,false,"nuova password");

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                email= null;
            } else {
                email= extras.getString("email");
            }
        } else {
            email= (String) savedInstanceState.getSerializable("email");
        }

        mPassword = (TextView) findViewById(R.id.attrNuovaPassword);
        Button memorizza = (Button) findViewById(R.id.insert_new_password_button);
        memorizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memorizzaNewPassword();
            }
        });

    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
    private void memorizzaNewPassword() {
        String password = mPassword.getText().toString();

        mPassword.setError(null);
        Log.d(TAG,password);
        if (!isPasswordValid(password)) {
            mPassword.setError(getString(R.string.error_incorrect_password));
            mPassword.requestFocus();
        }
        else {
            UtentiFactory.getInstance().changePassword(email,password);

            Toast.makeText(RecuperoPassword.this,"Password cambiata con successo",Toast.LENGTH_LONG).show();

            Intent i = new Intent(RecuperoPassword.this,Login.class);
            startActivity(i);
            finish();
        }
    }

}
